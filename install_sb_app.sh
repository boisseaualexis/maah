#! /bin/bash
sudo apt-get update
sudo apt-get install -y maven unzip
sudo wget https://static.javatpoint.com/springboot/download/spring-boot-application-run.zip
sudo unzip spring-boot-application-run.zip
sudo mvn --file spring-boot-application-run/ clean install
java -jar spring-boot-application-run/target/spring-boot-application-run-0.0.1-SNAPSHOT.jar