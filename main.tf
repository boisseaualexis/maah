### Author(s) : BOISSEAU Alexis
### Date : 02-10-2021
### School : EPISEN Créteil

################# Data Pipeline Course - Getting started with TERRAFORM #################

### Create a VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}
###

### Create public subnets
# 'Main' Subnet
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
availability_zone = var.aws_av_zone

  tags = {
    Name = "main"
  }
}

# 'Secondary' subnet
resource "aws_subnet" "secondary" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
availability_zone = var.aws_av_zone

  tags = {
    Name = "secondary"
  }
}
###

### Create an internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}
###

### Create a route for the igw
resource "aws_route_table" "igw_route" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "igw_route"
  }
}
###

### Create route association
#resource "aws_route_table_association" "a" {
#  subnet_id      = aws_subnet.main.id
#  route_table_id = aws_route_table.igw_route.id
#}
resource "aws_main_route_table_association" "a" {
  vpc_id         = aws_vpc.main.id
  route_table_id = aws_route_table.igw_route.id
}
###

### Create an instance with Apache web server on main subnet
resource "aws_instance" "terraform" {
  ami           = "ami-03d5c68bab01f3496"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.main.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.allow_http80.id]
  associate_public_ip_address = true
  key_name = "boisseau-dette"
  user_data = "${file("install_apache.sh")}"
  
  tags = {
    Name = "terraformtest"
  }
}
###

### Create an instance with SpringBoot test app on secondary subnet
resource "aws_instance" "terraformsb" {
  ami           = "ami-03d5c68bab01f3496"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.secondary.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.allow_http8080.id]
  associate_public_ip_address = true
  key_name = "boisseau-dette"
  user_data = "${file("install_sb_app.sh")}"

  tags = {
    Name = "terraformSB"
  }
}
###

### Create a security group for allowing ssh
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
###

### Create a security group for allowing http on port 80
resource "aws_security_group" "allow_http80" {
  name        = "allow_http80"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http80"
  }
}
###

### Create a security group for allowing http on port 8080
resource "aws_security_group" "allow_http8080" {
  name        = "allow_http8080"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "http"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http8080"
  }
}
###