### Author(s) : BOISSEAU Alexis
### Date : 02-10-2021
### School : EPISEN Créteil

################# Data Pipeline Course - Getting started with TERRAFORM #################

# Configure the AWS Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region     = var.aws_region
  profile    = "builderterraform"
  shared_credentials_file = "%USERPROFILE%\\.aws\\credentials"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
  # token      = var.aws_session_token
}