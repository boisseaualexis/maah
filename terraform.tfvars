### Author(s) : BOISSEAU Alexis
### Date : 02-10-2021
### School : EPISEN Créteil

################# Data Pipeline Course - Getting started with TERRAFORM #################

# AWS Settings
aws_region="us-west-2"
aws_av_zone="us-west-2a"
#aws_access_key_id="***"
#aws_secret_access_key="***"
#aws_session_token="***"

# Bucket Settings
bucket_name="episenbaresultsbucket" # must be unique in the world