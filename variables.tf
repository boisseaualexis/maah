### Author(s) : BOISSEAU Alexis
### Date : 02-10-2021
### School : EPISEN Créteil

################# Data Pipeline Course - Getting started with TERRAFORM #################

variable "aws_access_key_id" {
  type = string
  description = "AWS access key"
}
variable "aws_secret_access_key" {
  type = string
  description = "AWS secret key"
}
variable "aws_region" {
  type = string
  description = "AWS region"
}

variable "aws_av_zone" {
  type = string
  description = "AWS AZ"
}

#variable "aws_session_token" {
#  type = string
#  description = "AWS token"
#}

#variable "bucket_name" {
#  type = string
#  description = "Name of the s3 bucket to create"
#}